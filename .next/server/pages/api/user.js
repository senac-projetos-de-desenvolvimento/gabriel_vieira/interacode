"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/user";
exports.ids = ["pages/api/user"];
exports.modules = {

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("mongodb");

/***/ }),

/***/ "./pages/api/user.tsx":
/*!****************************!*\
  !*** ./pages/api/user.tsx ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _utils_database__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../utils/database */ \"./utils/database.ts\");\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{\n    if (req.method === 'POST') {\n        const { name , email , senha  } = req.body;\n        if (!name || !email || !senha) {\n            res.status(400).json({\n                error: 'Missing body parameter'\n            });\n            return;\n        }\n        const { db  } = await (0,_utils_database__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n        const response = await db.collection('users').insertOne({\n            name,\n            email,\n            senha\n        });\n        res.status(200).json(response.ops[0]);\n    } else {\n        res.status(400).json({\n            error: 'Wrong request method'\n        });\n    }\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9hcGkvdXNlci50c3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDMEM7QUFhMUMsaUVBQU0sT0FDSkMsR0FBbUIsRUFDbkJDLEdBQTZELEdBQzNDLENBQUM7SUFDbkIsRUFBRSxFQUFFRCxHQUFHLENBQUNFLE1BQU0sS0FBSyxDQUFNLE9BQUUsQ0FBQztRQUMxQixLQUFLLENBQUMsQ0FBQyxDQUFDQyxJQUFJLEdBQUVDLEtBQUssR0FBRUMsS0FBSyxFQUFDLENBQUMsR0FBR0wsR0FBRyxDQUFDTSxJQUFJO1FBRXZDLEVBQUUsR0FBR0gsSUFBSSxLQUFLQyxLQUFLLEtBQUtDLEtBQUssRUFBRSxDQUFDO1lBQzlCSixHQUFHLENBQUNNLE1BQU0sQ0FBQyxHQUFHLEVBQUVDLElBQUksQ0FBQyxDQUFDO2dCQUFDQyxLQUFLLEVBQUUsQ0FBd0I7WUFBQyxDQUFDO1lBQ3hELE1BQU07UUFDUixDQUFDO1FBRUQsS0FBSyxDQUFDLENBQUMsQ0FBQ0MsRUFBRSxFQUFDLENBQUMsR0FBRyxLQUFLLENBQUNYLDJEQUFPO1FBRTVCLEtBQUssQ0FBQ1ksUUFBUSxHQUFHLEtBQUssQ0FBQ0QsRUFBRSxDQUFDRSxVQUFVLENBQUMsQ0FBTyxRQUFFQyxTQUFTLENBQUMsQ0FBQztZQUN2RFYsSUFBSTtZQUNKQyxLQUFLO1lBQ0xDLEtBQUs7UUFDUCxDQUFDO1FBRURKLEdBQUcsQ0FBQ00sTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDRyxRQUFRLENBQUNHLEdBQUcsQ0FBQyxDQUFDO0lBQ3JDLENBQUMsTUFBTSxDQUFDO1FBQ05iLEdBQUcsQ0FBQ00sTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7WUFBQ0MsS0FBSyxFQUFFLENBQXNCO1FBQUMsQ0FBQztJQUN4RCxDQUFDO0FBQ0gsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzL2FwaS91c2VyLnRzeD8zMWM1Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5leHRBcGlSZXF1ZXN0LCBOZXh0QXBpUmVzcG9uc2UgfSBmcm9tICduZXh0JztcbmltcG9ydCBjb25uZWN0IGZyb20gJy4uLy4uL3V0aWxzL2RhdGFiYXNlJztcblxuaW50ZXJmYWNlIEVycm9yUmVzcG9uc2VUeXBlIHtcbiAgZXJyb3I6IHN0cmluZztcbn1cblxuaW50ZXJmYWNlIFN1Y2Nlc3NSZXNwb25zZVR5cGUge1xuICBfaWQ6IHN0cmluZztcbiAgbmFtZTogc3RyaW5nO1xuICBlbWFpbDogc3RyaW5nO1xuICBzZW5oYTogc3RyaW5nO1xufVxuXG5leHBvcnQgZGVmYXVsdCBhc3luYyAoXG4gIHJlcTogTmV4dEFwaVJlcXVlc3QsXG4gIHJlczogTmV4dEFwaVJlc3BvbnNlPEVycm9yUmVzcG9uc2VUeXBlIHwgU3VjY2Vzc1Jlc3BvbnNlVHlwZT5cbik6IFByb21pc2U8dm9pZD4gPT4ge1xuICBpZiAocmVxLm1ldGhvZCA9PT0gJ1BPU1QnKSB7XG4gICAgY29uc3QgeyBuYW1lLCBlbWFpbCwgc2VuaGEgfSA9IHJlcS5ib2R5O1xuXG4gICAgaWYgKCFuYW1lIHx8ICFlbWFpbCB8fCAhc2VuaGEpIHtcbiAgICAgIHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgZXJyb3I6ICdNaXNzaW5nIGJvZHkgcGFyYW1ldGVyJyB9KTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25zdCB7IGRiIH0gPSBhd2FpdCBjb25uZWN0KCk7XG5cbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGRiLmNvbGxlY3Rpb24oJ3VzZXJzJykuaW5zZXJ0T25lKHtcbiAgICAgIG5hbWUsXG4gICAgICBlbWFpbCxcbiAgICAgIHNlbmhhLFxuICAgIH0pO1xuXG4gICAgcmVzLnN0YXR1cygyMDApLmpzb24ocmVzcG9uc2Uub3BzWzBdKTtcbiAgfSBlbHNlIHtcbiAgICByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9yOiAnV3JvbmcgcmVxdWVzdCBtZXRob2QnIH0pO1xuICB9XG59O1xuIl0sIm5hbWVzIjpbImNvbm5lY3QiLCJyZXEiLCJyZXMiLCJtZXRob2QiLCJuYW1lIiwiZW1haWwiLCJzZW5oYSIsImJvZHkiLCJzdGF0dXMiLCJqc29uIiwiZXJyb3IiLCJkYiIsInJlc3BvbnNlIiwiY29sbGVjdGlvbiIsImluc2VydE9uZSIsIm9wcyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/api/user.tsx\n");

/***/ }),

/***/ "./utils/database.ts":
/*!***************************!*\
  !*** ./utils/database.ts ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ connect)\n/* harmony export */ });\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ \"mongodb\");\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);\n\nconst client = new mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient(process.env.DATABASE_URL, {\n    useNewUrlParser: true,\n    useUnifiedTopology: true\n});\nasync function connect() {\n    if (!client.connect()) await client.connect();\n    const db = client.db('interacode').collection('users');\n    return {\n        db,\n        client\n    };\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi91dGlscy9kYXRhYmFzZS50cy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7QUFBaUQ7QUFPakQsS0FBSyxDQUFDQyxNQUFNLEdBQUcsR0FBRyxDQUFDRCxnREFBVyxDQUFDRSxPQUFPLENBQUNDLEdBQUcsQ0FBQ0MsWUFBWSxFQUFFLENBQUM7SUFDeERDLGVBQWUsRUFBRSxJQUFJO0lBQ3JCQyxrQkFBa0IsRUFBRSxJQUFJO0FBQzFCLENBQUM7QUFFYyxlQUFlQyxPQUFPLEdBQXlCLENBQUM7SUFDN0QsRUFBRSxHQUFHTixNQUFNLENBQUNNLE9BQU8sSUFBSSxLQUFLLENBQUNOLE1BQU0sQ0FBQ00sT0FBTztJQUUzQyxLQUFLLENBQUNDLEVBQUUsR0FBR1AsTUFBTSxDQUFDTyxFQUFFLENBQUMsQ0FBWSxhQUFFQyxVQUFVLENBQUMsQ0FBTztJQUNyRCxNQUFNLENBQUMsQ0FBQztRQUFDRCxFQUFFO1FBQUVQLE1BQU07SUFBQyxDQUFDO0FBQ3ZCLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi91dGlscy9kYXRhYmFzZS50cz82YWY5Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vbmdvQ2xpZW50LCBDb2xsZWN0aW9uIH0gZnJvbSAnbW9uZ29kYic7XG5cbmludGVyZmFjZSBDb25uZWN0VHlwZSB7XG4gIGRiOiBDb2xsZWN0aW9uO1xuICBjbGllbnQ6IE1vbmdvQ2xpZW50O1xufVxuXG5jb25zdCBjbGllbnQgPSBuZXcgTW9uZ29DbGllbnQocHJvY2Vzcy5lbnYuREFUQUJBU0VfVVJMLCB7XG4gIHVzZU5ld1VybFBhcnNlcjogdHJ1ZSxcbiAgdXNlVW5pZmllZFRvcG9sb2d5OiB0cnVlLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IGFzeW5jIGZ1bmN0aW9uIGNvbm5lY3QoKTogUHJvbWlzZTxDb25uZWN0VHlwZT4ge1xuICBpZiAoIWNsaWVudC5jb25uZWN0KCkpIGF3YWl0IGNsaWVudC5jb25uZWN0KCk7XG5cbiAgY29uc3QgZGIgPSBjbGllbnQuZGIoJ2ludGVyYWNvZGUnKS5jb2xsZWN0aW9uKCd1c2VycycpO1xuICByZXR1cm4geyBkYiwgY2xpZW50IH07XG59XG4iXSwibmFtZXMiOlsiTW9uZ29DbGllbnQiLCJjbGllbnQiLCJwcm9jZXNzIiwiZW52IiwiREFUQUJBU0VfVVJMIiwidXNlTmV3VXJsUGFyc2VyIiwidXNlVW5pZmllZFRvcG9sb2d5IiwiY29ubmVjdCIsImRiIiwiY29sbGVjdGlvbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./utils/database.ts\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/api/user.tsx"));
module.exports = __webpack_exports__;

})();