import { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Image from 'next/image';
import axios from 'axios';
import useSWR from 'swr';
import { useSession } from 'next-auth/client';

import api from '../utils/api';
import Nav from '../components/nav';
import Footer from '../components/footer';

const ProfilePage: NextPage = () => {
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [errorCount, setErrorCount] = useState(0);
  const [loggedUserWithoutAccount, setLoggedUserWithoutAccount] = useState(
    false
  );

  const [session, loading] = useSession();

  const { data, error } = useSWR(
    !loggedUserWithoutAccount && !loading
      ? `/api/user/${session?.user.email}`
      : null,
    api
  );

  useEffect(() => {
    setErrorCount((prevstate) => prevstate + 1);
    if (error && errorCount === 1) setLoggedUserWithoutAccount(true);
  }, [error, setErrorCount]);

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      await axios.post(`${process.env.NEXT_PUBLIC_URL}/api/user`, data);
      setLoggedUserWithoutAccount(false);
    } catch (err) {
      alert(
        err?.response?.data?.error || 'Houve um problema na criação da conta'
      );
    }
  };

  return (
    <div>
      <Nav />
      {!session && (
        <div className="text-3xl">
          Favor fazer login para acessar essa página <br />
        </div>
      )}
      {session && data && (
        <div>
          <p className="text-3xl border-2 border-box w-3/12 m-auto text-center mt-12">
            Olá, {data.data.name}
          </p>
          <p className="text-3xl w-3/12 m-auto text-center mt-2">
            E-mail: {data.data.email}
          </p>
          <div className="w-5/6 m-auto mt-12">
            <span className="font-bold text-2xl mr-2"></span>
            <span></span>
          </div>
          {loggedUserWithoutAccount && session && (
            <div className="flex flex-col items-center">
              <h1 className="text-3xl mt-20">Seja bem vindo ao Interacode!</h1>
              <h1 className="text-2xl">
                Por favor finalize a criação do seu perfil:
              </h1>
              <form
                onSubmit={handleSubmit}
                className="flex flex-col items-center"
              >
                <input
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  placeholder="Nome completo"
                  className="border-2 border-box w-full text-center my-4"
                />
                <input
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="E-mail"
                  className="border-2 border-box w-full text-center my-4"
                />
                <button
                  className="text-xl bg-box text-center text-white mt-8 p-2"
                  type="submit"
                >
                  Criar perfil
                </button>
              </form>
            </div>
          )}
          {loading && (
            <div className="text-5xl">
              <h1>CARREGANDO</h1>
            </div>
          )}

          <div className="text-center mb-10">
            <Image
              src="/logo3.png"
              alt="Logo Interacode"
              width={485}
              height={485}
            />
          </div>

          <Footer />
        </div>
      )}
      );
    </div>
  );
};

export default ProfilePage;
