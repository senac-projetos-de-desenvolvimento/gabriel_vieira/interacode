import { NextPage } from 'next';
import Image from 'next/image';
import styles from '../styles/Home.module.css';

import Nav from '../components/nav';
import Footer from '../components/footer';

const DesafioPage: NextPage = () => {
  return (
    <>
      <main className={styles.main}>
        <h1 className={styles.title}>Bem vindo aos desafios</h1>

        <p className={styles.description}>Escolha por onde quer começar</p>

        <div className={styles.grid}>
          <a href="#" className={styles.card}>
            <h2>HTML</h2>
            <p>Estruturação de conteúdo</p>
          </a>

          <a href="#" className={styles.card}>
            <h2>CSS</h2>
            <p>Estilização de conteúdos</p>
          </a>

          <a href="#" className={styles.card}>
            <h2>JavaScript</h2>
            <p>Dando vida ao conteúdo</p>
          </a>

          <a href="#" className={styles.card}>
            <h2>Diário</h2>
            <p>Todo dia um novo desafio.</p>
          </a>
        </div>
      </main>
      <Footer />
    </>
  );
};

export default DesafioPage;
