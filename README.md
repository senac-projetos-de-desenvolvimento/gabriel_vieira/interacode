# Interacode


## Como inicializar o projeto

- Crie seu próprio arquivo .env.local baseado em .env.example (crie uma instância mongoDB no MongoDB Atlas ou container docker e cole o url do banco de dados em .env.local, e também crie uma conta em auth0 e cole as variáveis AUTH0). 

- Instale as depêndencias com yarn:

`yarn`

- Inicie o servidor:

`yarn dev`
